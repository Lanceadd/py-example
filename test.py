# -*- coding:utf-8 -*-

example = {
    'a':1,
    'b':2,
    'c':3
}

# dict 1
for key, value in example.items():
    print("[key]" + key)
    print("[value]" + str(value))
    print("-"*100)

# dict 2
for key in example.keys():
    print('[key]' + key)
    print('*'*100)